#!/usr/bin/env python3
#
# Traite les données générés par __powerline_trace_*
#
# Concrètement, calcule le temps d'exécution à partir des dates successives et
# affiche le temps d'exécution de chaque commandes.

import logging
import os
import pdb
import sys


logger = logging.getLogger("perf")


def main(argv):
    log = argv[1]

    with open(log) as fo:
        for epoch, time, command in compute_command_time(parse_log(fo)):
            logger.debug("%f %s", time, command)


def parse_log(fo):
    for line in fo:
        try:
            epoch, command = line.split('+', 1)
        except ValueError:
            logger.debug("Sauter %r", line)
            continue
        epoch = float(epoch.replace(',', '.'))
        command = command.strip()
        yield epoch, command


def compute_command_time(commands):
    previous = next(commands)
    yield previous[0], 0, previous[1]
    for epoch, command in commands:
        time_ = epoch - previous[0]
        yield epoch, time_, command
        previous = epoch, command


class ErreurConnue(Exception):
    # Permet d'arrêter le programme de n'importe où.
    def __init__(self, message, exit_code=1):
        super(ErreurConnue, self).__init__(message)
        self.exit_code = exit_code


logging.basicConfig(
    format="%(levelname)1.1s: %(message)s",
    level=logging.DEBUG,
)

try:
    main(sys.argv[:])
    # Si tout se passe bien, terminer avec 0.
    exit(0)
except pdb.bdb.BdbQuit:
    logger.info("Fin de pdb.")
except ErreurConnue as e:
    logger.critical("%s", e)
    exit(e.exit_code)
except Exception:
    logger.exception("Erreur inconnue:")
    pdb.post_mortem(sys.exc_info()[2])

# Dans tout autre cas, annoncer l'erreur aux autre programmes.
exit(os.EX_SOFTWARE)
